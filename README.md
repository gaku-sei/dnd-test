## Here is the DnD test

### Installation

You need the last crystal binary installed on your machine (see [here](https://crystal-lang.org/docs/installation/) for more informations) and the `node`/`npm` binaries.

Then:

```sh
git clone https://gitlab.com/gaku-sei/dnd-test
npm i # or `yarn`
crystal deps
npm start # or `yarn start`
```

Both the server and the client will run. The running project is available at `http://localhost:8080` (the server may need time to start the first time).

N.B.: [yarn](https://yarnpkg.com/) is used under the hood to run tasks.

### Choices

#### Server

Ruby is the language used on the server for the moment, but it is way to slow to handle heavy WebSocket communications.

For such a task, three very efficient solutions are available right now:

- Erlang/Elixir and the de facto [Phoenix framework](http://phoenixframework.org/)
- JVM (Scala, Java, Kotlin or Clojure). [Akka](http://doc.akka.io/docs/akka-http/current/scala/http/) is a great tool, available in both Java and Scala, http-kit has great performances, and Kotlin is very trendy now on Android, attracting more and more developers
- C# and its solid ecosystem

All three have a very big downside for the DnDTest project; they are awefully heavy: heavy architecture, heavy tooling, heavy installation process, etc...

Remained the following lightweight-yet-powerful solutions:

- Node.JS robust and fast, yet less than the others (though [µWebSockets](https://github.com/uNetworking/uWebSockets) has amzing performances)
- Go which proposes a very efficient WebSocket [native library](https://godoc.org/golang.org/x/net/websocket) but is just syntactically 'meh' (and less type safe than many other compiled languages)
- Rust was a good candidate, yet a little bit _too_ complex
- Haskell which is the most type safe and elegant language, but syntactically to far from the reference (Ruby)
- Crystal is ultra lightweight, simple to install and to use and "mature" enough to provide a poweful WebSocket server (via [Kemal](http://kemalcr.com/))

Finally, Crystal was chosen mainly for its "typed Ruby" aspect but also for its very good performances.

#### Client

The client was easier to choose, we need React, so the solutions are basically:

- The very immature [ReasonML](https://reasonml.github.io/) (which supports JSX) [bindings](https://reasonml.github.io/reason-react/)
- An odd yet powerful [Scala](https://japgolly.github.io/scalajs-react/) library built on top of React
- One of the PureScript implementations of React ([Pux](http://purescript-pux.org/) was a serious candidate) but the drag n'drop part seems awefully complex in PureScript
- Barebone JavaScript, a little bit too dangerous for growing applications
- Flow or...
- TypeScript

TypeScript was chosen for its simplicity, the large amount definition files avaiblable and it's powerful type system.

#### Redux

Redux is way too heavy for such an application and was therefore dropped (see [here](https://medium.com/@dan_abramov/you-might-not-need-redux-be46360cf367)).

Nonetheless, Redux + [Redux-Observable](https://redux-observable.js.org/) is a good way to go for heavily event-oriented applications.

#### DB

DataBase was also useless in the given context. Nevertheless we can easily plug any kind of DB, or any kind of messagers (Kafka, RabbitMQ, ...) to the current code.

### Desktop != Mobile

To test the mobile "touch" events you can activate the Google Chrome's [Device Mode](https://developers.google.com/web/tools/chrome-devtools/device-mode/) and then _refresh_ your browser (the mobile/desktop test is done at launch only to avoid unnecessary checks during the application's lifetime).

#### Tests

You can type `KEMAL_ENV=test crystal spec` to run the tests.
