require "kemal"

require "./dnd/*"

# We set the CORS and JSON headers middlewares
add_handler CORSHeaders.new
add_handler JSONHeaders.new

bucket = SocketBucket.new
profiles = Profiles.all

# The WebSocket handler
ws "/socket/profiles" do |socket|
  bucket << socket

  # We send to current socket a welcoming message
  bucket.send_last(profiles.to_json)

  socket.on_message do |message|
    begin
      # Here the transaction mutates the object and is immediately done
      # We can easily change that : let's just push transactions in the profiles object,
      # send back the transaction to every sockets and let the client computes the new
      # profiles itself.
      profiles >> Matrix::Transaction.from_json(message)
      bucket.send_all(profiles.compute!.to_json)
    rescue
    end
  end

  socket.on_close do
    bucket.delete socket
  end
end

Kemal.run
