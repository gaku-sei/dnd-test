struct Int
  def times_to_array(&block : self -> T) : Array(T) forall T
    ret = [] of T

    self.times do |i|
      ret << block.call(i)
    end

    ret
  end
end
