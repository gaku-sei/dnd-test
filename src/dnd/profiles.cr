class Profiles
  class Profile
    JSON.mapping(
      id: Int32,
      avatar: String,
      name: String,
      position: String,
      scoring: Float64,
    )

    def_clone

    def_equals @id

    def initialize(@id, @avatar, @name, @position, @scoring)
    end
  end

  def_clone

  def_equals @matrix, @transactions

  def initialize(@matrix : Matrix(Profile), @transactions : Array(Matrix::Transaction) = [] of Matrix::Transaction)
  end

  # Fake in memory "all" method
  # In real code all would fetch from db the profiles
  def self.all(cols : UInt32 = 2u32) : Profiles
    Profiles.new(matrix: Fake.profiles_matrix(cols))
  end

  def to_json
    @matrix.to_json
  end

  def compute : Profiles
    clone.compute!
  end

  def compute! : Profiles
    @transactions.each do |transaction|
      @matrix.move!(transaction)
    end

    @transactions = [] of Matrix::Transaction

    self
  end

  def >>(t : Matrix::Transaction) : Profiles
    @transactions << t
    self
  end
end
