class Array(T)
  def safely_delete_at(index : Int32) : T | Nil
    index < size ? delete_at(index) : nil
  end

  def safely_insert(index : Int32, value : T) : Array(T) | Nil
    index <= size ? insert(index, value) : nil
  end
end
