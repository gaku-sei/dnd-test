class CORSHeaders < Kemal::Handler
  def call(context)
    context.response.headers.add("Access-Control-Allow-Origin", "*")
    call_next context
  end
end

class JSONHeaders < Kemal::Handler
  def call(context)
    context.response.content_type = "application/json"
    context.response.headers.add("Accept", "application/json")
    call_next context
  end
end
