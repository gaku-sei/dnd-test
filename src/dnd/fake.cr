module Fake
  extend self

  def profiles_matrix(cols : UInt32) : Matrix(Profiles::Profile)
    Matrix(Profiles::Profile).new(cols: cols).fill!([
      [
        Profiles::Profile.new(
          id: 1,
          avatar: "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Steve_Jobs_Headshot_2010-CROP.jpg/1200px-Steve_Jobs_Headshot_2010-CROP.jpg",
          name: "Steve Jobs",
          position: "Producteur de pommes",
          scoring: 3.6,
        ),
        Profiles::Profile.new(
          id: 2,
          avatar: "https://pbs.twimg.com/profile_images/889736688624312321/xVAFH9ZH_400x400.jpg",
          name: "Bill Gates",
          position: "Installateur de fenêtres",
          scoring: 3.9,
        ),
      ],
      [
        Profiles::Profile.new(
          id: 3,
          avatar: "https://tctechcrunch2011.files.wordpress.com/2012/09/mark.jpeg",
          name: "Mark Zuckerberg",
          position: "Traffiquant d'âmes",
          scoring: 1.2,
        ),
      ],
    ])
  end
end
