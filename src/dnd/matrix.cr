class Matrix(T)
  alias Point = NamedTuple(x: UInt32, y: UInt32)

  alias Transaction = NamedTuple(from: Point, to: Point)

  @cols : UInt32
  @value : Array(Array(T))

  def_clone

  def_equals @cols, @value

  def initialize(@cols)
    @value = Array(Array(T)).new(@cols) { [] of T }
  end

  private def point_is_invalid(point : Point) : Bool
    point[:x] >= @cols
  end

  def [](point : Point) : T | Nil
    return nil if point_is_invalid(point)

    @value[point[:x]].at(point[:y].to_i32) { nil }
  end

  def delete_at!(point : Point, value : T | Nil) : self | Nil
    return nil if point_is_invalid(point)

    @value[point[:x]].safely_delete_at(point[:y].to_i32)

    self
  end

  def insert!(point : Point, value : T | Nil) : self | Nil
    return nil if point_is_invalid(point)

    x = point[:x]

    y = point[:y] > @value[x].size ? @value[x].size : point[:y]

    @value[x].safely_insert(y.to_i32, value)

    self
  end

  def fill!(value : Array(Array(T))) : self
    value.each_index do |i|
      value[i].each_index do |j|
        @value[i] << value[i][j]
      end if i < @cols
    end

    self
  end

  def move!(transaction : Transaction) : self | Nil
    from = transaction[:from]
    to = transaction[:to]

    return nil if point_is_invalid(from) || point_is_invalid(to)
    
    target = self[from]

    return nil if target.nil?

    delete_at!(from, nil)

    insert!(to, target)

    self
  end

  def to_json
    @value.to_json
  end
end
