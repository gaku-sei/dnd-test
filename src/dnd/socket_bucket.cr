class SocketBucket
  include Enumerable(HTTP::WebSocket)

  @sockets = [] of HTTP::WebSocket

  def <<(socket : HTTP::WebSocket)
    @sockets << socket
  end

  def each
    @sockets.each do |socket|
      yield socket
    end
  end

  def delete(socket : HTTP::WebSocket)
    @sockets.delete socket
  end

  def send_last(message)
    @sockets.last.send message
  end

  def send_all(message, except sockets : Array(HTTP::WebSocket) = [] of HTTP::WebSocket)
    @sockets.each do |socket|
      socket.send message if !sockets.includes?(socket)
    end
  end
end
