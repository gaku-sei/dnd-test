import * as React from "react";

import { ID, webSocketURL } from "../constants";
import { Point, Profiles, Transaction } from "../types";
import header from "./header";
import ProfilePanel from "./ProfilePanel";

interface State {
  profiles?: Profiles;
  socket?: WebSocket;
}

export default class extends React.Component<{}, State> {
  state: State = {};

  onDrop = (panelId: ID, profileId: number): void => {
    const { profiles, socket } = this.state;

    if (!profiles || !socket) {
      return;
    }

    let from: Point | null = null;

    profiles.forEach((row, i) => {
      row.forEach(({ id }, j) => {
        if (id === profileId) {
          from = { x: i, y: j };
        }
      });
    });

    if (from === null) {
      return;
    }

    const panelLength = profiles[panelId].length;

    // Due to a complex TS bug (https://github.com/Microsoft/TypeScript/issues/11498)
    // we need to cast from to Point
    const to = { x: panelId, y: panelId === (from as Point).x ? panelLength - 1 : panelLength };

    socket.send(JSON.stringify({ from, to } as Transaction));
  };

  async componentDidMount() {
    const socket = new WebSocket(webSocketURL);

    socket.onopen = () => {
      this.setState({ socket });
    };

    socket.onmessage = ({ data }) => {
      try {
        this.setState({
          profiles: JSON.parse(data) as Profiles,
        });
      } catch {
        // NoOp
      }
    }
  }

  render() {
    const { profiles, profiles: [left = [], right = []] = [] } = this.state;

    return (
      <div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
        {header}
        <main className="mdl-layout__content">
          <div className="page-content">
            {!profiles ? null : (
              <div className="mdl-grid">
                <ProfilePanel
                  position={ID.Left}
                  onDrop={this.onDrop}
                  profiles={left}
                  title="À RENCONTRER"
                />
                <ProfilePanel
                  position={ID.Right}
                  onDrop={this.onDrop}
                  profiles={right}
                  title="ENTRETIEN"
                />
              </div>
            )}
          </div>
        </main>
      </div>
    );
  }
}
