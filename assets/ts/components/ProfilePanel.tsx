import * as classNames from "classnames";
import * as React from "react";
import {
  ConnectDropTarget,
  DropTarget, DropTargetConnector, DropTargetMonitor,
} from "react-dnd";

import { DraggableItems, ID } from "../constants";
import { Profile as ProfileType } from "../types";
import Profile from "./Profile";

interface Props {
  position: ID;
  profiles: ProfileType[];
  title: string;
  onDrop(panelId: ID, profileId: number): void;
}

interface DropTargetProps {
  connectDropTarget: ConnectDropTarget;
  isOver: boolean;
}

const target = {
  drop({ position, onDrop }: Props, monitor: DropTargetMonitor) {
    onDrop(position, (monitor.getItem() as any).id);
  },
};

const collect = (connect: DropTargetConnector, monitor: DropTargetMonitor): DropTargetProps => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
});

const ProfilePanel = ({ connectDropTarget, isOver: over, profiles, title }: Props & DropTargetProps) => {
  const className = classNames("panel mdl-cell mdl-cell--6-col mdl-cell--12-col-tablet mdl-shadow--2dp", { over });

  return connectDropTarget(
    <div className={className}>
      <div className="title">
        <h4><strong><span className="mdl-badge" data-badge={profiles.length}>{title}</span></strong></h4>
      </div>
      <div>
        {profiles.map(profile => (
          <Profile key={profile.id} {...profile} />
        ))}
      </div>
    </div>
  );
};

export default DropTarget<Props>(DraggableItems.Profile, target, collect)(ProfilePanel);
