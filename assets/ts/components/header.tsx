import * as React from "react";

export default (
  <header className="mdl-layout__header">
    <div className="mdl-layout__header-row">
      <span className="mdl-layout-title">Stage - Account Manager</span>
    </div>
  </header>
);
