import * as classNames from "classnames";
import * as React from "react";
import {
  ConnectDragSource,
  DragSource, DragSourceConnector, DragSourceMonitor,
} from "react-dnd";

import { DraggableItems } from "../constants";
import { Profile as ProfileType } from "../types";

interface Props extends ProfileType {
}

interface DragSourceProps {
  connectDragSource: ConnectDragSource;
  isDragging: boolean;
}

const source = {
  beginDrag({ id }: Props) {
    return { id };
  },
};

const collect = (connect: DragSourceConnector, monitor: DragSourceMonitor): DragSourceProps => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging(),
});

type AllProps = Props & DragSourceProps;

class Profile extends React.Component<AllProps> {
  shouldComponentUpdate({ id, isDragging }: AllProps) {
    return id !== this.props.id || isDragging !== this.props.isDragging;
  }

  render() {
    const { avatar, connectDragSource, isDragging: dragging, name, position, scoring } = this.props;

    const className = classNames("profile mdl-cell mdl-cell--12-col mdl-card mdl-shadow--2dp", { dragging });

    return connectDragSource(
      <div className={className}>
        <div className="mdl-card__title mdl-card--expand">
          <div className="body mdl-grid">
            <div className="mdl-cell mdl-cell--4-col mdl-cell--middle">
              <img className="avatar" src={avatar} />
            </div>
            <div className="mdl-cell mdl-cell--8-col mdl-cell--middle">
              <h4>{name}</h4>
            </div>
          </div>
        </div>
        <div className="mdl-card__actions mdl-card--border">
          <div className="mdl-card__title">
            <span className="mdl-chip">
              <span className="mdl-chip__text">{scoring}</span>
            </span>
            &nbsp;
            <span>{position}</span>
          </div>
        </div>
      </div>
    );
  }
}

export default DragSource<Props>(DraggableItems.Profile, source, collect)(Profile);
