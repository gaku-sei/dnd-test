// An Isomorphic language/framework (Such as Node.JS, Scala, or PureScript)
// would allow us to use the Transaction and Point types in both client and server
export interface Point {
  x: number;
  y: number;
}

export interface Transaction {
  from: Point;
  to: Point;
}

export interface Profile {
  id: number;
  avatar: string;
  name: string;
  position: string;
  scoring: number;
}

export type Profiles = Profile[][];
