export const webSocketURL = "ws://localhost:3000/socket/profiles";

export const enum DraggableItems {
  Profile = "Profile",
}

export const enum ID {
  Left,
  Right,
}
