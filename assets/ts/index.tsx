import * as React from "react";
import { render } from "react-dom";
import { DragDropContext } from "react-dnd";
import HTML5Backend from "react-dnd-html5-backend";
import TouchBackend from "react-dnd-touch-backend";

import App from "./components/App";

// Very, very, very quick and dirty way to know if the client is a tablet/mobile
const keyboardLess = /mobi/i.test(window.navigator.userAgent);
const DnDApp = DragDropContext(keyboardLess ? TouchBackend : HTML5Backend)(App);

render(
  <DnDApp />,
  document.getElementById("app"),
);
