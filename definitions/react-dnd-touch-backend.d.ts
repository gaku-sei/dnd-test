declare module "react-dnd-touch-backend" {
  import * as ReactDnd from "react-dnd";

  export namespace NativeTypes {
      const FILE: string;
      const URL: string;
      const TEXT: string;
  }

  export function getEmptyImage(): any; // Image
  export default class TouchBackend implements ReactDnd.Backend {}
}
