const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: [
    'core-js',
    'material-design-lite',
    path.join(__dirname, 'assets', 'ts', 'index'),
    path.join(__dirname, 'assets', 'scss', 'main.scss'),
  ],

  output: {
    path: path.join(__dirname, 'public', 'ts'),
    filename: 'bundle.js',
  },

  resolve: {
    extensions: ['.js', '.ts', '.jsx', '.tsx'],
  },

  plugins: [
    new ExtractTextPlugin('styles.css'),
  ],

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        loader: 'ts-loader',
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader',
        }),
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'sass-loader'],
        }),
      },
    ],
  },
};
