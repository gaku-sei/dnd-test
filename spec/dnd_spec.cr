require "./spec_helper"

describe Dnd do
  describe Matrix do
    it "should be able to move target to point in simple matrix" do
      matrix_base = Matrix(String).new(cols: 2u32)

      matrix = matrix_base.clone.fill!([
        ["toto", "tutu"],
      ])

      transaction = {
        from: {x: 0u32, y: 0u32},
        to: {x: 1u32, y: 0u32},
      }

      matrix_result = matrix_base.clone.fill!([
        ["tutu"],
        ["toto"],
      ])

      matrix.move!(transaction).should eq(matrix_result)
      matrix.should eq(matrix_result)
    end

    it "should be able to move target to point" do
      matrix_base = Matrix(String).new(cols: 4u32)

      matrix = matrix_base.clone.fill!([
        ["toto", "tutu"],
        ["tata"],
        [] of String,
        ["titi"],
      ])

      transaction = {
        from: {x: 0u32, y: 0u32},
        to: {x: 2u32, y: 0u32},
      }

      matrix_result = matrix_base.clone.fill!([
        ["tutu"],
        ["tata"],
        ["toto"],
        ["titi"],
      ])

      matrix.move!(transaction).should eq(matrix_result)
      matrix.should eq(matrix_result)
    end

    it "should be able to move target to point even at the end" do
      matrix_base = Matrix(String).new(cols: 5u32)

      matrix = matrix_base.clone.fill!([
        ["toto", "tutu"],
        ["tata"],
        [] of String,
        ["titi"],
      ])

      transaction = {
        from: {x: 1u32, y: 0u32},
        to: {x: 4u32, y: 0u32},
      }

      matrix_result = matrix_base.clone.fill!([
        ["toto", "tutu"],
        [] of String,
        [] of String,
        ["titi"],
        ["tata"],
      ])

      matrix.move!(transaction).should eq(matrix_result)
      matrix.should eq(matrix_result)
    end

    it "should be able to move target to point even if the column does not exist yet" do
      matrix_base = Matrix(String).new(cols: 7u32)

      matrix = matrix_base.clone.fill!([
        ["toto", "tutu"],
        ["tata"],
        [] of String,
        ["titi"],
      ])

      transaction = {
        from: {x: 1u32, y: 0u32},
        to: {x: 6u32, y: 0u32},
      }

      matrix_result = matrix_base.clone.fill!([
        ["toto", "tutu"],
        [] of String,
        [] of String,
        ["titi"],
        [] of String,
        [] of String,
        ["tata"],
      ])

      matrix.move!(transaction).should eq(matrix_result)
      matrix.should eq(matrix_result)
    end

    it "should be able to move target to point even if the row does not exist yet event squashing spaces" do
      matrix_base = Matrix(String).new(cols: 4u32)

      matrix = matrix_base.clone.fill!([
        ["toto", "tutu"],
        ["tata"],
        [] of String,
        ["titi"],
      ])

      matrix_result = matrix_base.clone.fill!([
        ["toto", "tutu"],
        [] of String,
        [] of String,
        ["titi", "tata"],
      ])

      transaction = {
        from: {x: 1u32, y: 0u32},
        to: {x: 3u32, y: 2u32},
      }

      matrix.move!(transaction).should eq(matrix_result)
      matrix.should eq(matrix_result)
    end

    it "should return nil if transaction is wrong for target" do
      matrix = Matrix(String).new(cols: 4u32).fill!([
        ["toto", "tutu"],
        ["tata"],
        [] of String,
        ["titi"],
      ])

      matrix_clone = matrix.clone

      transaction = {
        from: {x: 1u32, y: 1u32},
        to: {x: 4u32, y: 0u32},
      }

      matrix_clone.move!(transaction).should eq(nil)
      matrix_clone.should eq(matrix)
    end
  end

  describe Profiles do
    it "should accept some transactions and compute a new Profiles object" do
      profiles = Profiles.all(cols: 3u32)
      clone = profiles.clone

      transaction1 = {
        from: {x: 0u32, y: 0u32},
        to: {x: 2u32, y: 0u32},
      }

      transaction2 = {
        from: {x: 0u32, y: 0u32},
        to: {x: 1u32, y: 0u32},
      }

      expected_profiles = Profiles.new(
        matrix: Matrix(Profiles::Profile).new(cols: 3u32).fill!([
          [] of Profiles::Profile,
          [
            Profiles::Profile.new(
              id: 2,
              avatar: "https://pbs.twimg.com/profile_images/889736688624312321/xVAFH9ZH_400x400.jpg",
              name: "Bill Gates",
              position: "Installateur de fenêtres",
              scoring: 3.9,
            ),
            Profiles::Profile.new(
              id: 3,
              avatar: "https://tctechcrunch2011.files.wordpress.com/2012/09/mark.jpeg",
              name: "Mark Zuckerberg",
              position: "Traffiquant d'âmes",
              scoring: 1.2,
            ),
          ],
          [
            Profiles::Profile.new(
              id: 1,
              avatar: "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Steve_Jobs_Headshot_2010-CROP.jpg/1200px-Steve_Jobs_Headshot_2010-CROP.jpg",
              name: "Steve Jobs",
              position: "Producteur de pommes",
              scoring: 3.6,
            ),
          ],
        ]),
      )

      profiles >> transaction1 >> transaction2

      profiles.compute.should eq(expected_profiles)
      profiles.compute.should_not eq(clone)
    end
  end
end
